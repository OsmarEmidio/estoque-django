from django.http import JsonResponse
from django.shortcuts import render, redirect, resolve_url, get_object_or_404
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.views.generic import CreateView, ListView, UpdateView, DeleteView
from .models import Produto, Estoque, EstoqueItens, EstoqueEntrada, EstoqueSaida
from .forms import ProdutoForm, EstoqueForm, EstoqueItensForm, UsuarioForm
from django.http import HttpResponseRedirect
from django.forms import inlineformset_factory
from django.views.generic import ListView, DetailView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.contrib.auth.views import PasswordChangeView
from django.contrib.auth.forms import PasswordChangeForm


# Create your views here.

def login_user(request):
    return render(request, 'app/login.html')

@csrf_protect
def submit_login(request):
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/')
        else:
            messages.error(request, 'Usuario e/ou senha errados')
    return redirect('/')

@login_required(login_url='/login')
def home(request):
    objects = Produto.objects.all()
    search = request.GET.get('search')
    if search:
        objects = objects.filter(nome__icontains=search)
    context = {'object_list': objects}
    return render (request, 'app/produto_list.html', context)

def logout_user(request):
    logout(request)
    return redirect('/login')

@login_required(login_url='/login')
def produto_list(request):
    total = 0
    objects = Produto.objects.all()
    search = request.GET.get('search')
    if search:
        objects = objects.filter(nome__icontains=search)
    context = {'object_list': objects}
    context['total'] = "aaaaaaaa" #str(total)
    return render (request, 'app/produto_list.html', context)

@login_required(login_url='/login')
def produto(request, id):
    obj = Produto.objects.get(id=id)
    context = {'object': obj}
    return render (request, 'app/produto.html', context)

@login_required(login_url='/login')
def produto_register(request):
    product_id = request.GET.get('id')
    if product_id:
        produto = Produto.objects.get(id=product_id)
        return render(request, 'app/produto_form.html', {'produto':produto})
    return render(request, 'app/produto_form.html')

@login_required(login_url='/login')
def produto_delete(request, id):
    produto = Produto.objects.get(id=id)
    produto.delete()
    return redirect('/products')

@login_required(login_url='/login')
def set_produto(request):
    nome = request.POST.get('nome')
    codigo = request.POST.get('codigo')
    marca = request.POST.get('marca')
    quantidade = request.POST.get('quantidade')
    preco = request.POST.get('preco')
    p_id= request.POST.get('produto_id')
    if p_id:
        produto = Produto.objects.get(id=p_id)
        produto.nome = nome
        produto.codigo = codigo
        produto.marca = marca
        produto.quantidade = quantidade
        produto.preco = preco
        produto.save()
    else:
        produto = Produto.objects.create(nome=nome, codigo=codigo, marca=marca, quantidade=quantidade, preco=preco)
    url = '/products/{}' .format(produto.id)
    return redirect(url)

# @login_required(login_url='/login')
# def estoque_entrada_list(request):
#     template_name = 'app/estoque_entrada_list.html'
#     objects = EstoqueEntrada.objects.all()
#     context={'object_list': objects}
#     return render(request, template_name, context)

class EstoqueEntradaList(LoginRequiredMixin, ListView):
    model = EstoqueEntrada
    template_name = 'app/estoque_entrada_list.html'
    paginate_by = 10

    def get_queryset(self):
        query = self.request.GET.get('search')
        if query:
            object_list = self.model.objects.filter(created__icontains=query)
        else:
            object_list = self.model.objects.all()
        return object_list

@login_required(login_url='/login')
def estoque_entrada_detail(request, id):
    template_name = 'app/estoque_entrada_detail.html'
    obj = EstoqueEntrada.objects.get(id=id)
    context={'object': obj}
    return render(request, template_name, context)

class EstoqueEntradaDetail(LoginRequiredMixin, DetailView):
    model = EstoqueEntrada
    template_name = 'app/estoque_entrada_detail.html'

    def get_context_data(self, **kwargs):
        context = super(EstoqueEntradaDetail, self).get_context_data(**kwargs)
        context['url_list'] = 'estoque_entrada'
        return context

@login_required(login_url='/login')
def estoque_entrada_add(request):
    template_name = 'app/estoque_entrada_form.html'
    estoque_form = Estoque()
    item_estoque_formset = inlineformset_factory(
        Estoque,
        EstoqueItens,
        form=EstoqueItensForm,
        extra=0,
        can_delete=False,
        min_num=1,
        validate_min=True,
    )
    if request.method == 'POST':
        form = EstoqueForm(request.POST, instance=estoque_form, prefix='main')
        formset = item_estoque_formset(
            request.POST,
            instance=estoque_form,
            prefix='estoque'
        )
        if form.is_valid() and formset.is_valid():
            form = form.save()
            form.movimento='e'
            form.save()
            formset.save()
            dar_baixa_estoque(form)
            url = 'estoque_entrada_detail'
            return HttpResponseRedirect(resolve_url(url, form.pk))
    else:
        form = EstoqueForm(instance=estoque_form, prefix='main')
        formset = item_estoque_formset(instance=estoque_form, prefix='estoque')

    context = {'form': form, 'formset': formset}
    return render(request, template_name, context)

@login_required(login_url='/login')
def produto_json(request, pk):
    ''' Retorna o produto, id e estoque. '''
    produto = Produto.objects.filter(pk=pk)
    data = [item.to_dict_json() for item in produto]
    return JsonResponse({'data': data})

def dar_baixa_estoque(form):
    produtos=form.estoques.all()
    for item in produtos:
        produto = Produto.objects.get(pk=item.produto.pk)
        produto.quantidade = item.saldo
        produto.save()

# @login_required(login_url='/login')
# def estoque_saida_list(request):
#     template_name = 'app/estoque_saida_list.html'
#     objects = EstoqueSaida.objects.all()
#     context={'object_list': objects}
#     return render(request, template_name, context)

class EstoqueSaidaList(LoginRequiredMixin, ListView):
    model = EstoqueSaida
    template_name = 'app/estoque_saida_list.html'
    paginate_by = 10

    def get_context_data(self, *args, **kwargs):
        total = 0
        for objeto in self.model.objects.all():
            for obj in objeto.estoques.all():
                total += obj.quantidade * obj.produto.preco
        context = super().get_context_data(*args, **kwargs)
        context['total'] = total
        return context

    def get_queryset(self):
        query = self.request.GET.get('search')
        if query:
            object_list = self.model.objects.filter(created__icontains=query)
        else:
            object_list = self.model.objects.all()
        return object_list

@login_required(login_url='/login')
def estoque_saida_detail(request, id):
    template_name = 'app/estoque_saida_detail.html'
    obj = EstoqueSaida.objects.get(id=id)
    context={'object': obj}
    return render(request, template_name, context)

@login_required(login_url='/login')
def estoque_saida_add(request):
    template_name = 'app/estoque_saida_form.html'
    estoque_form = Estoque()
    item_estoque_formset = inlineformset_factory(
        EstoqueSaida,
        EstoqueItens,
        form=EstoqueItensForm,
        extra=0,
        can_delete=False,
        min_num=1,
        validate_min=True,
    )
    if request.method == 'POST':
        form = EstoqueForm(request.POST, instance=estoque_form, prefix='main')
        formset = item_estoque_formset(
            request.POST,
            instance=estoque_form,
            prefix='estoque'
        )
        if form.is_valid() and formset.is_valid():
            form = form.save()
            form.movimento='s'
            form.save()
            formset.save()
            dar_baixa_estoque(form)
            url = 'estoque_saida_detail'
            return HttpResponseRedirect(resolve_url(url, form.pk))
    else:
        form = EstoqueForm(instance=estoque_form, prefix='main')
        formset = item_estoque_formset(instance=estoque_form, prefix='estoque')

    context = {'form': form, 'formset': formset}
    return render(request, template_name, context)

class ProdutoList(LoginRequiredMixin, ListView):
    model = Produto
    template_name = 'app/produto_list.html'
    paginate_by = 10

    def get_context_data(self, *args, **kwargs):
        total = 0
        for i in self.model.objects.all():
            x = i.quantidade * i.preco
            total = total + x
        context = super().get_context_data(*args, **kwargs)
        context['total'] = total
        return context

    def get_queryset(self):
        query = self.request.GET.get('search')
        if query:
            object_list = self.model.objects.filter(nome__icontains=query)
        else:
            object_list = self.model.objects.all()
        return object_list

class UsuarioCadastro(LoginRequiredMixin, CreateView):
    template_name = 'app/cadastro_usuario.html'
    form_class = UsuarioForm
    success_url = reverse_lazy('users')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['titulo'] = "Cadastrar Usuário"
        context['botao'] = "Cadastrar"
        return context

class UsuarioUpdate(LoginRequiredMixin, UpdateView):
    template_name = 'app/cadastro_usuario.html'
    model = User
    fields = ['first_name', 'last_name', 'username', 'email', 'is_superuser', 'is_active']
    success_url = reverse_lazy('products')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['titulo'] = "Editar Usuário"
        context['botao'] = "Editar"
        return context

@login_required(login_url='/login')
def usuario_delete(request, pk):
    user = User.objects.get(pk=pk)
    user.delete()
    return redirect('/usuarios')

class UsuariosList(LoginRequiredMixin, ListView):
    model = User
    template_name = 'app/users_list.html'
    paginate_by = 10

    def get_queryset(self):
        query = self.request.GET.get('search')
        if query:
            object_list = self.model.objects.filter(username__icontains=query)

        else:
            object_list = self.model.objects.all()
        return object_list

class PaginaUsuario(LoginRequiredMixin, DetailView):
    model = User
    template_name = "app/pagina_usuario.html"

    def get_context_data(self, **kwargs):
        users = User.objects.all()
        saida = EstoqueSaida.objects.filter(usuario=self.kwargs['pk'])
        entrada = EstoqueEntrada.objects.filter(usuario=self.kwargs['pk'])
        context = super(PaginaUsuario, self).get_context_data(**kwargs)
        page_user = get_object_or_404(User, id=self.kwargs['pk'])
        usuario = self.request.user
        context['user'] = usuario
        context['saida'] = saida
        context['entrada'] = entrada
        context['page_user'] = page_user
        return context

# class MyCreateView(LoginRequiredMixin, CreateView):
#     model = Profile
#     form_class = ProfileForm

#     def form_valid(self, form):
#         obj = form.save(commit=False)
#         obj.user = get_object_or_404(Profile, user=self.kwargs['user'])
#         return super(MyCreateView, self).form_valid(form)

#     def get_success_url(self):
#          return reverse_lazy('users')

# class MyDetailView(DetailView):
#     model = Profile

#     def get_object(self):
#         return get_object_or_404(Profile, username=self.kwargs['username'])

#     def get_success_url(self):
#         return reverse_lazy('detail', kwargs={"username": self.request.user.username})

class PasswordsChangeView(LoginRequiredMixin, PasswordChangeView):
    from_class = PasswordChangeForm
    success_url = reverse_lazy('products')