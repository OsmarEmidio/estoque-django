from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path
from app import views
from django.contrib.staticfiles.urls import static, staticfiles_urlpatterns
from . import settings

urlpatterns = [
    path('admin/', admin.site.urls, name='admin'),
    path('login/', views.login_user, name='login'),
    path('login/submit', views.submit_login),
    path('logout/', views.logout_user, name='logout'),
    path('', views.ProdutoList.as_view(), name='home'),
    path('products/', views.ProdutoList.as_view(), name='products'),
    path('products/<int:id>/', views.produto, name='produto'),
    path('products/register/', views.produto_register, name='produto_register'),
    path('products/register/submit', views.set_produto),
    path('products/delete/<slug:id>/', views.produto_delete),
    path('products/<int:pk>/json/', views.produto_json, name='produto_json'),
    path('estoque/entrada', views.EstoqueEntradaList.as_view(), name='estoque_entrada'),
    path('estoque/entrada/<int:id>/', views.estoque_entrada_detail, name='estoque_entrada_detail'),
    path('estoque/entrada/add/', views.estoque_entrada_add, name='estoque_entrada_add'),
    path('estoque/saida/', views.EstoqueSaidaList.as_view(), name='estoque_saida'),
    path('estoque/saida/<int:id>/', views.estoque_saida_detail, name='estoque_saida_detail'),
    path('estoque/saida/add/', views.estoque_saida_add, name='estoque_saida_add'),
    path('usuario/registrar/', views.UsuarioCadastro.as_view(), name='usuario_registrar'),
    path('usuario/editar/<int:pk>', views.UsuarioUpdate.as_view()),
    path('usuario/deletar/<int:pk>/', views.usuario_delete),
    path('usuarios/', views.UsuariosList.as_view(), name='users'),
    path('usuario/<int:pk>', views.PaginaUsuario.as_view(), name='userpage'),
    path('usuario/password', views.PasswordsChangeView.as_view(template_name='app/mudar_senha.html'))
    #path('/perfil<str:username>', views.MyDetailView.as_view(), name="detail"),
    #path('perfil/<str:user>', views.MyCreateView.as_view(), name="detail")
]

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
