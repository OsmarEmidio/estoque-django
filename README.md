**Windows**

```
pip install virtualenv 

python -m venv .venv

.venv\Scripts\activate

pip install -r requirements.txt

cd estoque

python manage.py migrate

python manage.py createsuperuser

python manage.py runserver
```

**Ubuntu Linux**

```
sudo apt-get update

sudo apt-get install python3-venv

source .venv/bin/activate

sudo apt install python3-pip

pip3 install -r requirements.txt

python3 manage.py migrate

python manage.py createsuperuser

python manage.py runserver
```
